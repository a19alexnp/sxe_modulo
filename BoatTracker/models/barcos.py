
import logging
from odoo import models,fields,api
from datetime import date,datetime
from odoo.exceptions import UserError
from odoo.tools.translate import _

logger = logging.getLogger(__name__)

class Barcos(models.Model):
    _name="barcos"
    _rec_name="nombre"

    nombre=fields.Char("Nombre",required=True)
    imagen=fields.Binary("Foto")
    eslora=fields.Float("Eslora",required=True)
    currency_id = fields.Many2one(
        'res.currency', string='Currency')
    precio_dia=fields.Monetary("precio",required=True)
    fecha_llegada=fields.Date("Fecha Llegada",default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    fecha_ultimo_mantenimiento=fields.Date("Ultimo Mantenimiento",default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    categoria=fields.Selection([
        ("velero","Velero"),
        ("motor","Motor")
    ])
    patron=fields.Many2many("patron",string="Patron")
    estado=fields.Selection([
        ("noDisponible","No disponible"),
        ("disponible","Disponible"),
        ("reservado","Reservado"),
        ("alquilado","Alquilado"),
        ("reparacion","Reparacion")
    ],
    "Estado",default="noDisponible")

    
    _sql_constraints=[
        ('name_uniq', 'unique(titulo)','El Barco ya existe!'),
        ('eslora_nonegativa','check(eslora >0)','Debes introducir un valor positivo'),
        ('precio_positivo','check(precio_dia >0)','El precio tiene que ser superior a 0')
        ]

    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [('noDisponible', 'disponible'),
                   ('disponible', 'noDisponible'),
                   ('reservado', 'disponible'),
                   ('disponible', 'reservado'),
                   ('reservado', 'alquilado'),
                   ('alquilado', 'disponible'),
                   ("reparacion","disponible"),
                   ("disponible","reparacion")]
        return (old_state, new_state) in allowed
        
    @api.multi
    def change_state(self, new_state):
        for barco in self:
            if barco.is_allowed_transition(barco.estado, new_state):
                barco.estado = new_state
            else:
                message =  _('Pasar del estado %s al estado %s no está permitido') % (barco.estado, new_state)
                raise UserError(message)

    def hacer_disponible(self):
        self.change_state("disponible")

    def hacer_noDisponible(self):
        self.change_state("noDisponible")

    def hacer_reservado(self):
        self.change_state("reservado")
    
    def hacer_alquilado(self):
        self.change_state("alquilado")

    def hacer_reparacion(self):
        self.change_state("reparacion")



class Patron(models.Model):
    _name="patron"
    _inherit="res.partner"

    dias_de_mar=fields.Integer("Dias de mar",required=True)
    
