
from odoo import models,fields,api
from datetime import date,datetime
from odoo.exceptions import UserError



class Viajes(models.Model):
    _name="viajes"
    

    barco=fields.Many2one("barcos",String="Barco")
    numero_pasajeros=fields.Integer("Cantidad De Pasajeros")
    fecha_salida=fields.Date("Dia de incio",default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    fecha_regreso=fields.Date("Dia de regreso",default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    currency_id = fields.Many2one(
        'res.currency', string='Currency')

    @api.multi
    @api.depends('barco','fecha_salida','fecha_regreso')
    def compute_precio(self):
        salida=10000*self.fecha_salida.year+100*self.fecha_salida.month+self.fecha_salida.day
        regreso=10000*self.fecha_regreso.year+100*self.fecha_regreso.month+self.fecha_regreso.day
        if salida==regreso:
            self.precio=self.barco.precio_dia
        else:
            self.precio=self.barco.precio_dia*(regreso-salida+1)

    precio=fields.Monetary("Precio total",compute="compute_precio",readonly=True)

    _sql_constraints=[
        ('pasajeros_maximo','check(numero_pasajeros <13)','Solo pueden viajar 12 personas')
        ]
    @api.model
    def create(self,vals):
        barco_rec=self.env['barcos'].browse(vals['barco'])
        barco_rec.hacer_reservado()
        res = super(Viajes, self).create(vals)
        return res
        