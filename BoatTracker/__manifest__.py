# -*- coding: utf-8 -*-
{
    'name': "Mis Barcos",  # Module title
    'summary': "Maneja la situacion de tus barcos facil",  # Module subtitle phrase
    'description': """Con este modulo de odoo podras manejar todos los barcos que tienes en alquiler""",  # You can also rst format
    'author': "Alex Nuñez Perez",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/barcos.xml',
        'views/patron.xml',
        'views/viajes.xml'
        
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
