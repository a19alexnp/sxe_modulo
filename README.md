# Modulo BoatTracker

Este es un modulo orientado a empresas de chárter naútico. Este modulo está pensado para que haga mas facil el control de todos tus barcos , patrones y las reservas.

# Funcionamiento

La vista principal del modulo es la de los barcos, aqui podas crear tus barcos añadiendo los patrones que tiene cada uno precio, dia de compra, de mantenimiento y mas campos básicos.

La vista de patrones es una vista heredada de res.partner con un campo más (dias_de_mar); este sirve para indicar la experiencia del patrón.

La tercera y última vista es la de los viajes. En esta seleccionaras el barco que va a ser reservado, indicarás los dias a reservar y este marcar solo el precio calculando la multiplicación de los dias que se alquila por el precio por dia de ese barco. A la vez cuando se hace una reserva, el barco se marca como reservado.

# Arquitectura

## El modelo Barcos 
 ### Tiene 10 campos:
    -Nombre: char
    -Imagen: binary
    -eslora: float -> tiene una constraint que impide que sea negativa
    -currency_id : heredado de res.currency que hace falta para tener un campo monetary
    -precio_dia : monetary
    -fecha_llegada: date
    -fecha_ultimo_mantenimiento: date
    -categoria: de selección, a escoger si el barco es velero o a motor ya que esto le puede interesar a mucha gente
    -patron: Many2many (modelo patron) -> según los metros que tenga el barco puede ser que se necesite un título u otro, por lo que en los barcos guardamos todos los patrones que pueda tener
    -estado: de seleccion -> sirve para indicar si el barco ya está alquilado, reservado, si va a ir a reparación, etc.

### Tiene tres constraint
 #### Antes nombramos una asi que vamos con las otras dos:
    - Una para que el nombre de cada barco sea único
    - Otra para que el precio sea positivo

### Metodos:
    -is_allowed_transition(self,old_state,new_state) ->aquí definimos que tipo de transiciones de estado se van a poder hacer
    -hacer_$estado(self) -> un método por cada estado que cambia el estado del barco 
    -change_state(self,new_state) -> comprueba que la transición de estado sea posible

### Vistas:

    Vista formulario, arbol, kanban y tiene filtros de busqueda

## El modelo Patrón:

Es un modelo heredado de res.parnert al que le modificamos las vistas para mostrar solo los campos que necesitamos. El único campo añadido es: dias_de_mar de tipo integer que nos sirve para guardas los dias de experiencia que tiene ese patrón.

### Vistas:

    Tiene vista formulario, kanban y arbol

## El model viajes

### Campos:
    -barco: many2one (modelo barcos)
    -numero_pasajeros: integer -> tiene una constraint ya que en un barco de charter puede llevar un máximo de 12 tripulantes
    -fecha_salida:date
    -fecha_regreso:date
    -currency_id: para lo mismo que en el modelo barcos
    -precio: atributo computado. Se define multiplicando los dias que se va de viaje por el precio del barco

### Funciones:

    - compute_precio(self): es la función que define el precio , haciendo la operaciṕn matemática antes mencionada
    - create(self): sobreescribimos esta función para que al crearse marque al barco como reservado

### Vistas

    Tiene una vista formulario, una arbol y una calendario (perfecta para saber cuando son los viajess)

